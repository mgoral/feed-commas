import pytest

import feedcommas.config


@pytest.fixture
def fake_config(monkeypatch, tmpdir):
    monkeypatch.setattr(feedcommas.config, '_cfg', None)
    monkeypatch.setattr(feedcommas.config, '_path', None)

    cfg_path = tmpdir.mkdir('config').join('config.ini')
    cfg_path.write('')

    feedcommas.config.set_path(str(cfg_path))
    return feedcommas.config.config()
