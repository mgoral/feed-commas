import feedcommas.config as config
import feedcommas.model as model


def test_empty_article_content():
    art = model.Article()
    assert art.orig_content is None
    assert art.content == ''


def test_article_content_filter(fake_config):
    orig_content = '<b>FooBar</b>'
    art = model.Article(orig_content=orig_content)

    config.set_value('settings.html-filter', 'builtin')
    assert art.content == 'FooBar'

    config.set_value('settings.html-filter', 'none')
    assert art.content == orig_content

    config.set_value('settings.html-filter', '')
    assert art.content == orig_content
