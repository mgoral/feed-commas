= Feed Commas 0.3.0 changelog

== Changes since 0.2.1:

=== New features

* Added offline mode
+
Offline mode replaces time-based requests caching. Feed Commas now manages its
own database of received articles which is displayed when there's no network
available, when CommaFeed doesn't respond or when offline mode is manually set
via a `offline-toggle` command.

* Added status line with the following options: `{notif}`, `{offline}` and
  `{unread}`.

* Added `sync-article-count` configuration option.

* Added `timeout` configuration option.

* Added `read-all` command.

=== Bug fixes

* Fixed a lot of crashes where configuration contained empty options.

=== Removed

* Removed `cache-time` config setting.

=== Other changes

* `workers` configuration entry moved to the `[settings]` section.

* Changed `toggle-read` and `toggle-star` commands to `read-toggle` and
  `star-toggle`.

* Changed default keybindings: `read-toggle` to `R`, `star-toggle` to `S` and
  `refresh` to `r`.
