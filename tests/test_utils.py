import os
import pytest

import feedcommas.utils as utils


opj = os.path.join


def test_get_password_cmd_ok():
    cmd = "echo stdout && echo stderr >&2"

    ok, out, err = utils.get_password_cmd(cmd)
    assert ok is True
    assert out == 'stdout'
    assert err == 'stderr'

    nook_cmd = "echo stdout && echo stderr >&2 && exit 1"
    ok, out, err = utils.get_password_cmd(nook_cmd)
    assert ok is False
    assert out == 'stdout'
    assert err == 'stderr'


def test_mkdir_p(tmpdir):
    dname = 'writable'
    tmpd = tmpdir.mkdir(dname)

    path = opj(str(tmpd), 'abc', 'foo', 'bar')
    assert utils.mkdir_p(path) is None
    assert os.path.isdir(path)

    # directory exists - check that it doesn't raise anything
    assert utils.mkdir_p(path) is None
    assert utils.mkdir_p(None) is None
