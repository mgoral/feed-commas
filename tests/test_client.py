import re
import collections
import pytest
import json

import responses
from responses import POST, GET

from functools import partial

from feedcommas.content import ReadType
import feedcommas.config as config
import feedcommas.client as client


@pytest.fixture
def client_init(fake_config):
    def _f(server):
        config.set_value('server.address', server)
        client.init('user', 'pass')
    return _f


def request_descr(api_method, status, resp_content=None):
    '''Helper function which prepares a description of a request and mocked
    response.'''
    Req = collections.namedtuple('Request', ['fn', 'method', 'api_method'])
    Resp = collections.namedtuple('Response', ['status', 'body', 'content_type'])
    Descr = collections.namedtuple('RequestDescr', ['req', 'resp'])

    FnD = collections.namedtuple('FunctionDesc', ['fn', 'method'])

    functions = {
        'user/login': FnD(partial(client.login, 'user', 'pass'), POST),
        'category/entries': FnD(partial(client.get_articles, 'category'), GET),
        'category/get': FnD(client.get_feeds, GET),
        'category/mark': FnD(partial(client.mark, 'category'), POST),
        'entry/star': FnD(client.star, POST),
        'feed/entries': FnD(partial(client.get_articles, 'feed'), GET),
        'feed/mark': FnD(partial(client.mark, 'feed'), POST),
    }

    content_type = 'text/plain'
    if isinstance(resp_content, dict):
        resp_content = json.dumps(resp_content)
        content_type = 'application/json'

    fnd = functions[api_method]
    req = Req(fnd.fn, fnd.method, api_method)
    resp = Resp(status, resp_content, content_type)
    return Descr(req, resp)


def generate_requests():
    return [
        request_descr('user/login', 200),

        request_descr('category/entries', 200, {}),
        request_descr('category/mark', 200),

        request_descr('feed/entries', 200, {}),
        request_descr('feed/mark', 200),

        request_descr('entry/star', 200),
    ]


@pytest.mark.parametrize('request_data', generate_requests())
@pytest.mark.parametrize('redirect', [
    ('http://example.com', 'http://www.example.com'),
    ('https://example.com', 'https://blah.example.com'),
])
def test_allowed_redirects(request_data, redirect, client_init):
    rd = request_data
    from_, to = redirect

    client_init(from_)

    with responses.RequestsMock(assert_all_requests_are_fired=True) as rsps:
        def _redirect(request):
            new_url = request.url.replace(from_, to, 1)
            headers = {'location': new_url}
            body = '<center>Moved Permanently</center>'

            rsps.add(rd.req.method, new_url,
                     status=rd.resp.status, body=rd.resp.body,
                     content_type=rd.resp.content_type)

            return (301, headers, body)

        url_match = re.compile(r'%s/rest/%s.*' % (from_, rd.req.api_method))
        rsps.add_callback(rd.req.method, url_match, callback=_redirect,
                          content_type='text/html')

        try:
            rd.req.fn()
        except client.APIFailure:
            pass  # disregard failures - we don't test them here

        assert len(rsps.calls) == 2

        assert rsps.calls[0].request.url.startswith(from_)
        assert not rsps.calls[0].request.url.startswith(to)

        assert rsps.calls[1].request.url.startswith(to)
        assert not rsps.calls[1].request.url.startswith(from_)


@pytest.mark.parametrize('request_data', generate_requests())
@pytest.mark.parametrize('redirect', [
    ('http://example.com', 'http://other.com'),
])
def test_disallowed_redirects(request_data, redirect, client_init):
    rd = request_data
    from_, to = redirect

    client_init(from_)

    with responses.RequestsMock(assert_all_requests_are_fired=False) as rsps:
        def _redirect(request):
            new_url = request.url.replace(from_, to, 1)
            headers = {'location': new_url}
            body = '<center>Moved Permanently</center>'

            rsps.add(rd.req.method, new_url,
                     status=rd.resp.status, body=rd.resp.body,
                     content_type=rd.resp.content_type)

            return (301, headers, body)

        url_match = re.compile(r'%s/rest/%s.*' % (from_, rd.req.api_method))
        rsps.add_callback(rd.req.method, url_match, callback=_redirect,
                          content_type='text/html')

        try:
            rd.req.fn()
        except client.APIFailure:
            pass  # disregard failures - we don't test them here

        assert len(rsps.calls) == 1

        assert rsps.calls[0].request.url.startswith(from_)
        assert not rsps.calls[0].request.url.startswith(to)
