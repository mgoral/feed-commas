= Feed Commas next changelog

== Changes since 0.2:

=== Bug fixes

* Fixed `-c, --config` option not working with some relative paths.

* First selected article (after refresh, Feed Commas start etc.) will be now
  correctly marked auto-read.

* Fixed a crash when marking an article while other articles are still loading.

=== Other changes

* Don't draw a read/unread checkbox for articles which are not markable.
