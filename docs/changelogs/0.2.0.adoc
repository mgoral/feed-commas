= Feed Commas 0.2.0 changelog

== Changes since 0.1:

=== New features

* Automatically invalidate cache from the older versions of Feed Commas.
+
This is backward-incompatible change. If you have cached results from any of
previous versions, you'll need to remove them before starting Feed Commas:
+
----
rm ~/.cache/feed-commas/*
----

* Display a name of a feed in article's footer.

* Opening an article in a browser automatically marks it as read.

* Set title of terminal window.

* When CommaFeed's credentials are missing, Feed Commas will show a modal dialog
  which allows filling missing credentials.
+
It's especially useful on the first run, when Feed Commas creates a
configuration with empty credentials and user would need to quit Feed Commas in
order to change and apply a correct configuration.

* Write a configuration file only if configuration was modified inside Feed
  Commas.

* Feed Commas accepts `-c,--config` switch to allow specifying configuration
  file.

=== Bug fixes

* Fixed detecting incorrect encoding in incoming JSON objects.

* Cache is correctly updated when marking articles.

* Fixed reading empty cache files.

* Fixed a crash when Feed Commas tried to save configuration file without
  sufficient permissions.

=== Other changes

* More eye-candy synchronization progress circle.
