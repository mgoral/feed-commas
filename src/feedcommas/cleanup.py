# Copyright (C) 2017 Michał Góral.
#
# This file is part of Feed Commas
#
# Feed Commas is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Feed Commas is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Feed Commas. If not, see <http://www.gnu.org/licenses/>.

'''Global during shutdown handler. Any client code might register cleanup
functions, which will be called upon a shutdown in order they were
registered.'''

import sys
import functools


class Cleanup:
    '''Cleanupper'''
    def __init__(self):
        self._clean = []
        self._lock = False

    def register(self, func, *args, **kwargs):
        '''Adds a cleanup function. It will be called with a given args and
        kwargs when the time will come. If cleanup is locked, raises
        RuntimeError'''
        if not self._lock:
            self._clean.append(functools.partial(func, *args, **kwargs))
        else:
            raise RuntimeError('Adding new cleanup functions locked')

    def register_last(self, func, *args, **kwargs):
        '''Same as `Cleanup.register`, but locks cleanup after registering
        `func` from registering additional cleanup handlers.'''
        self.register(func, *args, **kwargs)
        self._lock = True

    @property
    def locked(self):
        '''Returns whether Cleanup is locked or not'''
        return self._lock

    def start(self):
        '''Calls all handlers in order they were registered and unlocks a
        cleanup. All handlers are called, even if one of them raises an
        exception. Last exception raised is re-raised after that.'''
        einfo = None
        while self._clean:
            handler = self._clean.pop(0)
            try:
                handler()
            except:
                einfo = sys.exc_info()

        self._lock = False

        if einfo is not None:
            raise einfo[1]


cleanup = Cleanup()
