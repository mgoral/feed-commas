import os
import contextlib
import pytest

from feedcommas import config
from feedcommas.config import _path_seems_writable


opj = os.path.join


@contextlib.contextmanager
def chdir(path):
    saved = os.getcwd()
    os.chdir(path)
    yield
    os.chdir(saved)


@contextlib.contextmanager
def chmod(path, perms):
    saved = os.stat(path).st_mode
    os.chmod(path, perms)
    yield
    os.chmod(path, saved)


def test_path_seems_writable(tmpdir):
    # we assume that directory created by pytest is writable
    dname = 'writable'
    tmpd = tmpdir.mkdir(dname)
    f = tmpd.join('dummycfg')
    f.write('')

    assert _path_seems_writable(str(f))
    assert _path_seems_writable(opj(str(f), 'foo'))
    assert _path_seems_writable(opj(str(f), 'foo', 'bar', 'cfg.txt'))

    assert not _path_seems_writable('/')
    assert not _path_seems_writable(str(tmpd))

    with chmod(str(tmpd), 0o400):
        # Without this if, this assertion WOULD FAIL for root user, who have
        # access to everything.
        if os.access(str(tmpd), os.W_OK | os.X_OK) is False:
            assert not _path_seems_writable(str(f))

    with chdir(os.path.dirname(str(f))):
        assert _path_seems_writable(os.path.basename(str(f)))
        assert _path_seems_writable('blah.txt')
        assert _path_seems_writable(opj('foo', 'bar'))
        assert _path_seems_writable(opj('foo', 'bar', 'blah'))

        assert _path_seems_writable(opj('.', 'blah.txt'))
        assert _path_seems_writable(opj('.', 'foo', 'bar', 'blah'))
        assert _path_seems_writable(opj('..', dname, 'blah.txt'))
        assert _path_seems_writable(opj('..', dname, 'foo', 'bar', 'blah'))

        assert not _path_seems_writable(opj('..', dname))
        assert not _path_seems_writable('.')
        assert not _path_seems_writable('..')


class TestRemoveOldConfigurations:
    def setup(self):
        config._cfg = None
        config._path = None

    def test_options(self, tmpdir):
        after = self._usecfg(tmpdir, '''
            [server]
            empty-dummy-entry =
            address = localhost:1234
            this-is-dummy-entry = 1000!
        ''')
        assert 'empty-dummy-entry' not in after
        assert 'this-is-dummy-entry' not in after
        assert 'address' in after

    def test_sections(self, tmpdir):
        after = self._usecfg(tmpdir, '''
            [server]

            [dummy-section]
            dummy = 1234

            [my-section]
        ''')
        assert 'dummy' not in after
        assert '[my-section]' not in after
        assert '[server]' in after

    @staticmethod
    def _usecfg(tmpdir, content):
        cfgfile = tmpdir.join('dummycfg')
        cfgfile.write(content)
        config.set_path(str(cfgfile))
        config.write_config(config.config())
        return cfgfile.read()
