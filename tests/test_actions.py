from unittest.mock import Mock, call
import pytest

import feedcommas.actions as actions


@pytest.fixture
def manager():
    manager = Mock()
    manager.attach_mock(Mock(), 'foo')
    manager.attach_mock(Mock(), 'bar')
    manager.attach_mock(Mock(), 'baz')
    manager.attach_mock(Mock(), 'foobar')
    return manager


@pytest.fixture
def action_map(manager):
    return {
        'foo': manager.foo,
        'bar': manager.bar,
        'baz': manager.baz,
        'foobar': manager.foobar,
    }


@pytest.fixture
def command_map():
    return {
        'f': 'foo',
        'b': 'bar',
        'B': 'baz',
        'F': 'foobar'
    }


@pytest.fixture
def handler(action_map):
    ret = actions.CommandHandler()
    ret.register_actions(action_map)
    return ret


def test_command_handler_call(handler, manager):
    handler.call('foo')
    handler.call('baz')
    handler.call('foob')
    handler.call('foobar')
    handler.call('bar')
    assert manager.mock_calls == [call.foo(), call.baz(), call.foobar(),
                                  call.foobar(), call.bar()]


def test_command_handler_call_ambiguous(handler, manager):
    handler.call('fo')
    assert manager.mock_calls == []

    handler.call('ba')
    assert manager.mock_calls == []


def test_command_handler_get_abbrev_names(handler, manager):
    assert sorted(handler.get_abbrev_names('foo')) == ['foo', 'foobar']
    assert sorted(handler.get_abbrev_names('f')) == ['foo', 'foobar']
    assert sorted(handler.get_abbrev_names('foob')) == ['foobar']
    assert sorted(handler.get_abbrev_names('ba')) == ['bar', 'baz']
    assert sorted(handler.get_abbrev_names('')) == ['bar', 'baz', 'foo',
                                                    'foobar']
    assert handler.get_abbrev_names('ladsfhadsfhdsaf') == []
    assert handler.get_abbrev_names('foobarbaz') == []

    assert manager.mock_calls == []


def test_handle_key(command_map, action_map, manager):
    assert actions.handle_key('b', action_map, command_map) == \
        manager.bar.return_value
    assert manager.mock_calls == [call.bar()]

    manager.reset_mock()
    assert actions.handle_key('B', action_map, command_map) == \
        manager.baz.return_value
    assert manager.mock_calls == [call.baz()]

    manager.reset_mock()
    assert actions.handle_key('f', action_map, command_map) == \
        manager.foo.return_value
    assert manager.mock_calls == [call.foo()]

    manager.reset_mock()
    assert actions.handle_key('F', action_map, command_map) == \
        manager.foobar.return_value
    assert manager.mock_calls == [call.foobar()]


def test_handle_not_mapped_key(command_map, action_map, manager):
    default = Mock()

    assert actions.handle_key('u', action_map, command_map) == 'u'
    assert actions.handle_key('u', action_map, command_map, default) == \
        default.return_value
    assert manager.mock_calls == []
    assert default.called

    for key in 'fbFB':
        assert actions.handle_key(key, {}, command_map) == key
        assert actions.handle_key(key, action_map, {}) == key
        assert manager.mock_calls == []

        default.reset_mock()
        assert actions.handle_key(key, {}, command_map, default) == \
            default.return_value
        assert manager.mock_calls == []
        assert default.called

        default.reset_mock()
        assert actions.handle_key(key, action_map, {}, default) == \
            default.return_value
        assert manager.mock_calls == []
        assert default.called

    default.reset_mock()


def test_CommandMapping():
    mapping = actions.CommandMapping()
    assert mapping['nav-up'] == 'cursor up'
    assert mapping['nav-down'] == 'cursor down'
    assert mapping['nav-left'] == 'cursor left'
    assert mapping['nav-right'] == 'cursor right'
    assert mapping['Nav-Right'] == 'Nav-Right'
    assert mapping['foobar'] == 'foobar'
