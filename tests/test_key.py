import itertools
import pytest

from feedcommas.key import Key


@pytest.mark.parametrize('meta,ctrl,shift,joinch', [
    ('a', 'c', 's', '-'),
    ('meta', 'ctrl', 'shift', ' ')
])
def test_key_str(meta, ctrl, shift, joinch):
    assert str(Key()) == ''

    assert str(Key('a')) == 'a'
    assert str(Key('A')) == 'A'

    for perm in itertools.permutations([meta, ctrl, shift]):
        specials = joinch.join(perm)

        upper_comb = joinch.join((specials, 'A'))
        assert str(Key(upper_comb)) == 'meta ctrl shift A'

        lower_comb = joinch.join((specials, 'a'))
        assert str(Key(lower_comb)) == 'meta ctrl shift a'


def test_key_comparison():
    assert Key('a') == Key('a')
    assert Key('A') != Key('a')

    assert Key('a') > Key('A')
    assert Key('A') < Key('a')

    assert Key('c-s-a-A') == Key('a-s-c-A')
    assert Key('c-s-a-A') != Key('c-s-a-B')


def test_map_key():
    d = {Key('a-s-c-a'): 'foobar'}

    assert d[Key('a-s-c-a')] == 'foobar'
    assert d.get(Key('a-s-c-b')) is None
